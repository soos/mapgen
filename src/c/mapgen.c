//#include "snoise3.c"
#include <stdlib.h>
//#include <stdio.h>
#include <png.h>
#include <libconfig.h>
#include "simplex.h"


typedef struct {
  unsigned long scale;
  unsigned long weight;
} layer_conf_t;


typedef struct {
  int height;
  int r;
  int g;
  int b;
} palette_t;


typedef struct {
  unsigned long width;
  unsigned long height;
  unsigned char layers_len;
  layer_conf_t *layers;
  unsigned short palette_len;
  palette_t *palette;
  //NoiseContext nc;
  png_bytep image;
  unsigned char gradient[256];
  //TODO: calculate histogram during heightmap generation
  unsigned long histogram[256];
} mapgen_conf_t;


int mapgen_init (mapgen_conf_t **conf) {
  if (*conf != NULL) {
    printf("error: !null conf");
    return 0;
  }
  *conf = malloc(sizeof(**conf));
  if (*conf == NULL) {
    printf("error: conf malloc");
    return 0;
  }
  fflush(stdout);
  memset(*conf, 0, sizeof(**conf));
  //permsetup(&((*conf)->nc));
  setup();

  return 1;
}


int mapgen_read_config (mapgen_conf_t *conf, char *file_name) {
  config_t cfg; 
  config_setting_t *setting;
  int i;

  config_init(&cfg);
  if (!config_read_file(&cfg, file_name)) {
    printf("%s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
    config_destroy(&cfg);
    return 0;
  }

  config_lookup_int(&cfg, "width", (int *)&(conf->width));
  config_lookup_int(&cfg, "height", (int *)&(conf->height));

  conf->image = malloc(conf->height * conf->width * sizeof(png_byte));
  if (conf->image == NULL) {
    printf("malloc error\n");
    return 0;
  }

  setting = config_lookup(&cfg, "heightmap.layer");
  if(setting != NULL) {
    if (!(conf->layers_len = config_setting_length(setting))) {
      printf("error: empty layers\n");
    } else {
      conf->layers = malloc(conf->layers_len*sizeof(*(conf->layers)));

      for (i=0; i<conf->layers_len; ++i) {
        config_setting_t *layer = config_setting_get_elem(setting, i);

        if(!(config_setting_lookup_int(layer, "scale", (int *)&(conf->layers[i].scale)) &&
              config_setting_lookup_int(layer, "weight", (int *)&(conf->layers[i].weight)))) {
          printf("warning: layer description\n");
        }
      }
    }
  }

  setting = config_lookup(&cfg, "heightmap.palette");
  if(setting != NULL) {
    if (!(conf->palette_len = config_setting_length(setting))) {
      printf("warning: empty palette\n");
    } else {
      conf->palette = malloc(conf->palette_len*sizeof(*(conf->palette)));

      for (i=0; i<conf->palette_len; ++i) {
        config_setting_t *palette = config_setting_get_elem(setting, i);

        if(!(config_setting_lookup_int(palette, "height", &(conf->palette[i].height)) &&
              config_setting_lookup_int(palette, "r", &(conf->palette[i].r)) &&
              config_setting_lookup_int(palette, "g", &(conf->palette[i].g)) &&
              config_setting_lookup_int(palette, "b", &(conf->palette[i].b)))) {
          printf("warning: layer description");
          conf->palette[i].height = 0;
          conf->palette[i].r = 0;
          conf->palette[i].g = 0;
          conf->palette[i].b = 0;
        }
      }
    }
  }

  config_destroy(&cfg);

  printf("width: %ld\n", conf->width);
  printf("height: %ld\n", conf->height);
  for (i=0; i<conf->layers_len; ++i) {
    printf("layer[%d]: scale: %-3ld, weight: %-3ld\n", i, conf->layers[i].scale,
        conf->layers[i].weight);
  }
  for (i=0; i<conf->palette_len; ++i) {
    printf("palette[%d]: %-3d : (%-3d,%-3d,%-3d)\n", i, conf->palette[i].height,
        conf->palette[i].r,
        conf->palette[i].g,
        conf->palette[i].b);
  }

  return 1;
}


int mapgen_generate_gradient (mapgen_conf_t *conf) {
  int i, j;
  //TODO: optimize gradient filling
  for (i=0; i<conf->palette_len; ++i) {
    for (j=conf->palette[i].height; j<256; ++j) {
      conf->gradient[j] = i;
    }
  }
  return 1;
}


void write_row_callback (png_structp png_ptr, png_uint_32 row, int pass) {
  printf("write: %u\r", row);
}


void mapgen_save (mapgen_conf_t *conf, char *file_name) {
  FILE *fp;
  png_structp png_ptr;
  png_infop info_ptr;
  png_colorp palette;
  png_uint_32 i;
  png_bytep row_pointers[conf->height];

  fp = fopen(file_name, "wb");
  if (fp == NULL) {
    return;
  }

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png_ptr == NULL) {
    fclose(fp);
    return;
  }

  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) {
    fclose(fp);
    png_destroy_write_struct(&png_ptr,  NULL);
    return;
  }

  if (setjmp(png_jmpbuf(png_ptr))) {
    fclose(fp);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    return;
  }

  png_init_io(png_ptr, fp);

  png_set_write_status_fn(png_ptr, write_row_callback);

  /*
  png_set_filter(png_ptr, 0, PNG_FILTER_PAETH | PNG_FILTER_VALUE_PAETH);
  png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
  */

  png_set_IHDR(png_ptr, info_ptr, conf->width, conf->height, 8, PNG_COLOR_TYPE_PALETTE,
      PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  palette = (png_colorp) png_malloc(png_ptr, conf->palette_len * png_sizeof(png_color));
  for (i=0; i<conf->palette_len; ++i) {
    palette[i].red = conf->palette[i].r;
    palette[i].green = conf->palette[i].g;
    palette[i].blue = conf->palette[i].b;
  }
  png_set_PLTE(png_ptr, info_ptr, palette, conf->palette_len);

  png_write_info(png_ptr, info_ptr);

  if (conf->height > PNG_UINT_32_MAX/png_sizeof(png_bytep)) {
    png_error (png_ptr, "Image is too tall to process in memory");
  }

  for (i = 0; i < conf->height; i++) {
    row_pointers[i] = conf->image + i * conf->width;
  }

  png_write_image(png_ptr, row_pointers);
  png_write_end(png_ptr, info_ptr);
  png_free(png_ptr, palette);
  palette = NULL;
  png_destroy_write_struct(&png_ptr, &info_ptr);
  fclose(fp);
}


int mapgen_generate (mapgen_conf_t *conf) {
  png_uint_32 x,y;
  double s;
  unsigned char i;
  unsigned long weight_sum = 0;
  png_bytep image;

  for (i=0; i<conf->layers_len; ++i) {
    weight_sum += conf->layers[i].weight;
  }

  image = conf->image;

  int tmpmax = -1000;
  int tmpmin = 1000;
  int tmp;
  for (y=0; y<conf->height; ++y) {
    for (x=0; x<conf->width; ++x) {
      s = .0;
      for (i=0; i<conf->layers_len; ++i) {
        s += noise((double)x/conf->layers[i].scale, (double)y/conf->layers[i].scale) * (double)conf->layers[i].weight;
      }
      tmp = (255 * (s / (double)weight_sum));
      *image = conf->gradient[(png_byte)tmp];
      if (tmp>tmpmax) tmpmax = tmp;
      if (tmp<tmpmin) tmpmin = tmp;
      ++image;
    }
    printf("render: %u\r", y+1);
    fflush(stdout);
  }
  printf("\n");
  printf("( %d , %d )\n", tmpmin, tmpmax);

  return 1;
}


void mapgen_end (mapgen_conf_t *conf) {
  if (conf) {
    if (conf->image) free(conf->image);
    if (conf->layers) free(conf->layers);
    if (conf->palette) free(conf->palette);
    //if (conf->nc.perm) free(conf->nc.perm);
    //if (conf->nc.mperm) free(conf->nc.mperm);
    free(conf);
  }
}


/*
int mapgen_generate_row_callback() {}
int mapgen_write_row_callback() {}
int mapgen_error_handler() {}
*/


int main () {
  printf("libpng: %d\n", PNG_LIBPNG_VER);
  printf("libconfig: %d.%d.%d\n", LIBCONFIG_VER_MAJOR, LIBCONFIG_VER_MINOR, LIBCONFIG_VER_REVISION);

  mapgen_conf_t *conf = NULL;
  if (mapgen_init(&conf))
    if (mapgen_read_config(conf, "mapgen.cfg"))
      if (mapgen_generate_gradient(conf))
        if (mapgen_generate(conf))
          /*
          generate_heighmap
          generate_histogram
          apply_gradient
          apply_biome
          */
          mapgen_save(conf, "map.png");
  mapgen_end(conf);

  return 0;
}
