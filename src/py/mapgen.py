import pyopencl as cl
import numpy as np
import png

'''
  Pipeline:
    1 pass = terrain heightmap ( mountains, sand, water, deep water )
    2 pass = meadows heightmap ( meadows )
    3 pass = clay heightmap ( clay )
    4 pass = biom heightmap ( coniferous forest, greenwood )
'''

'''
layer:
  [x offset, y offset, x scale, y scale, weight]
gradient:
  [[min height value, max height value), palette color number]
'''

water = {
  'layers' : [
    [   0,   0, 256, 256,  6 ],
    [   0,   0, 128, 128,  4 ],
    [   0,   0,  64,  64,  2 ],
    [   0,   0,  32,  32,  1 ]
  ],
  'gradient' : [
    [  0,  90, 1], # deep water
    [ 90,  93, 2], # shallow water
    [ 93,  95, 3], # sand
    [180, 256, 4], # mountain
  ]
}

meadow = {
  'layers' : [
    [ 90,  90, 300, 300, 1],
  ],
  'gradient' : [
    [220, 256, 5], # meadow
  ]
}

biom = {
  'layers' : [
    [0, 0, 300, 300, 4],
    [0, 0, 150, 150, 1],
  ],
  'gradient' : [
    [  0, 160, 6], # coniferous forest
    [160, 256, 7], # greenwood
  ]
}

palette = [
  (  0,  0,  0,  0),
  (  0,  0,100,255),
  (100,100,255,255),
  (200,200,  0,255),
  ( 50, 50, 50,255),
  (255,150,  0,255),
  (  0,100,  0,255),
  (  0,255,  0,255)
]


class Mapgen:
  def __init__(self, x, y):
    self.sx = np.uintc(x)
    self.sy = np.uintc(y)
    self.land = np.zeros(self.sx * self.sy, np.ubyte)
    self.ctx = cl.create_some_context()
    self.que = cl.CommandQueue(self.ctx)
    src = open("simplex.cl", "r").read()
    self.prg = cl.Program(self.ctx, src).build()

  def add(self, params):
    layers = params['layers']
    gradient = params['gradient']
    grad = np.zeros(256, np.ubyte)
    for i in range(len(gradient)):
      if gradient[i][0] > gradient[i][1]:
        print("warning: gradient", i, "inversion")
      grad[gradient[i][0]:gradient[i][1]] = gradient[i][2]

    xoffs = np.empty(len(layers), np.uintc)
    yoffs = np.empty(len(layers), np.uintc)
    xscales = np.empty(len(layers), np.uintc)
    yscales = np.empty(len(layers), np.uintc)
    weights = np.empty(len(layers), np.uintc)
    l_cl = np.uintc(len(layers))
    for i in range(len(layers)):
      xoffs[i] = layers[i][0]
      yoffs[i] = layers[i][1]
      xscales[i] = layers[i][2]
      yscales[i] = layers[i][3]
      weights[i] = layers[i][4]

    mf = cl.mem_flags
    land_cl = cl.Buffer(self.ctx, flags=mf.WRITE_ONLY | mf.COPY_HOST_PTR, hostbuf=self.land)
    grad_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=grad)
    xoffs_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=xoffs)
    yoffs_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=yoffs)
    xscales_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=xscales)
    yscales_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=yscales)
    weights_cl = cl.Buffer(self.ctx, flags=mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=weights)
    
    self.prg.noise(self.que, self.land.shape, None, land_cl, grad_cl, xoffs_cl, yoffs_cl, xscales_cl, yscales_cl, weights_cl, l_cl, self.sx)
    cl.enqueue_copy(self.que, self.land, land_cl).wait()

  def set_palette(self, palette):
    #TODO: check that len(palette) == colors count
    self.palette = palette

  def save(self, file_name):
    png_file = open(file_name, "wb")
    png_writer = png.Writer(self.sx, self.sy, palette=self.palette)
    png_writer.write(png_file, np.reshape(self.land, (self.sx, self.sy)))


if __name__ == '__main__':
  mapgen = Mapgen(1000, 1000)
  mapgen.add(water)
  mapgen.add(meadow)
  mapgen.add(biom)
  mapgen.set_palette(palette)
  mapgen.save("map.png")
